<?php
/**
 * MedTrainer
 * @copyright 2018 MedTrainer.com
 * @author Ricardo Ruiz <rruiz@medtrainer.com>
 */
namespace  MedCore;

/**
 * --------------------------------------------------------------------------
 * Company -> Child Resources
 * --------------------------------------------------------------------------
 *
 */
class Company extends MedCoreResource
{
    /**
     * @inheritDoc
     */
    public $resourceKey = 'companie';

    /**
     * @inheritDoc
     */
    protected $childResource = array(
        'CompanySettings' => 'CompanySettings'
    );
}