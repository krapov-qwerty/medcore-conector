<?php
/**
 * MedTrainer
 * @copyright 2018 MedTrainer.com
 * @author Ricardo Ruiz <rruiz@medtrainer.com>
 */
namespace  MedCore;

/**
 * --------------------------------------------------------------------------
 * Company -> Child Resources
 * --------------------------------------------------------------------------
 *
 */
class CompanySettings extends MedCoreResource
{
    /**
     * @inheritDoc
     */
    public $resourceKey = 'company_setting';
}