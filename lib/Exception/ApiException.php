<?php
/**
 * MedTrainer
 * @copyright 2018
 * @author Ricardo Ruiz <rruiz@medtrainer.com>
 */
namespace MedCore\Exception;

/**
 * Class ApiException
 * {@inheritdoc}
 * @package MedCore\Exception
 */
class ApiException extends \Exception
{

}