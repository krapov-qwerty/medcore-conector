<?php
/**
 * MedTrainer
 * @copyright 2018
 * @author Ricardo Ruiz <rruiz@medtrainer.com>
 */
namespace MedCore\Exception;

/**
 * Class CurlException
 * {@inheritdoc}
 * @package MedCore\Exception
 */
class CurlException extends \Exception
{

}