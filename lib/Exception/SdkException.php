<?php
/**
 * MedTrainer
 * @copyright 2018
 * @author Ricardo Ruiz <rruiz@medtrainer.com>
 */
namespace MedCore\Exception;

/**
 * Class SdkException
 * {@inheritdoc}
 * @package MedCore\Exception
 */
class SdkException extends \Exception
{
}