<?php
/**
 * MedTrainer
 * @copyright 2018 MedTrainer.com
 * @author Ricardo Ruiz <rruiz@medtrainer.com>
 */
namespace  MedCore;
/*
|--------------------------------------------------------------------------
| MedCore API SDK Client Class
|--------------------------------------------------------------------------
| This class initializes the resource objects
*/

/**
 * Class MedConnector
 * @package MedCore
 * @property-read $company
 * @method Company Company(integer $id = null)
 */
class MedConnector
{
    /**
     * @var float microtime of last api call
     */
    public static $microtimeOfLastApiCall;

    /**
     * @var float Minimum gap in seconds to maintain between 2 api calls
     */
    public static $timeAllowedForEachApiCall = .5;

    /**
     *API configurations
     *
     * @var array
     */
    public static $config = array();
    /**
     * List of available resources which can be called from this client
     *
     * @var string[]
     */
    protected $resources = array(
        'Company'
    );

    /**
     * MedConnector constructor.
     * @param array $config
     * @return void
     */
    public function __construct($config = array())
    {
        if (!empty($config)) {
            MedConnector::$config = $config;
            MedConnector::setAdminUrl();
        }
    }
    /**
     * Set the admin URL, based on the configured
     * @return string
     */
    public static function setAdminUrl() : string
    {
        $medURL = self::$config['url'];
        $medURL = preg_replace('#^https?://|/$#', '', $medURL);
        self::$config['AdminUrl'] = $medURL."/api/";
        return $medURL;
    }
    /**
     * Configure the SDK client
     * @param array $config
     * @return MedConnector
     */
    public static function config($config)
    {
        foreach ($config as $key => $value) {
            self::$config[$key] = $value;
        }
        //Re-set the admin url if shop url is changed
        if(isset($config['url'])) {
            self::setAdminUrl();
        }
        //If want to keep more wait time than .5 seconds for each call
        if (isset($config['AllowedTimePerCall'])) {
            static::$timeAllowedForEachApiCall = $config['AllowedTimePerCall'];
        }
        return new MedConnector;
    }
    /**
     * Maintain maximum 2 calls per second to the API
     * @param bool $firstCallWait Whether to maintain the wait time even if it is the first API call
     */
    public static function checkApiCallLimit($firstCallWait = false)
    {
        $timeToWait = 0;
        if (static::$microtimeOfLastApiCall == null) {
            if ($firstCallWait) {
                $timeToWait = static::$timeAllowedForEachApiCall;
            }
        } else {
            $now = microtime(true);
            $timeSinceLastCall = $now - static::$microtimeOfLastApiCall;
            //Ensure 2 API calls per second
            if($timeSinceLastCall < static::$timeAllowedForEachApiCall) {
                $timeToWait = static::$timeAllowedForEachApiCall - $timeSinceLastCall;
            }
        }
        if ($timeToWait) {
            //convert time to microseconds
            $microSecondsToWait = $timeToWait * 1000000;
            //Wait to maintain the API call difference of .5 seconds
            usleep($microSecondsToWait);
        }

        static::$microtimeOfLastApiCall = microtime(true);
    }
    /**
     * Return MedCoreResource instance for a resource.
     * @example $medcore->Company->get(); //Returns all available Company
     * Called like an object properties (without parenthesis)
     *
     * @param string $resourceName
     *
     * @return ShopifyResource
     */
    public function __get($resourceName)
    {
        return $this->$resourceName();
    }
    /**
     * Return MedCoreResource instance for a resource.
     * Called like an object method (with parenthesis) optionally with the resource ID as the first argument
     * @example $medcore->Company($productID); //Return a specific product defined by $productID
     *
     * @param string $resourceName
     * @param array $arguments
     *
     * @throws SdkException if the $name is not a valid ShopifyResource resource.
     *
     * @return MedCoreResource
     */
    public function __call($resourceName, $arguments)
    {
        if (!in_array($resourceName, $this->resources)) {
            if (isset($this->childResources[$resourceName])) {
                $message = "$resourceName is a child resource of " . $this->childResources[$resourceName] . ". Cannot be accessed directly.";
            } else {
                $message = "Invalid resource name $resourceName. Pls check the API Reference to get the appropriate resource name.";
            }
            throw new SdkException($message);
        }
        $resourceClassName = __NAMESPACE__ . "\\$resourceName";
        //If first argument is provided, it will be considered as the ID of the resource.
        $resourceID = !empty($arguments) ? $arguments[0] : null;
        //Initiate the resource object
        $resource = new $resourceClassName($resourceID);
        return $resource;
    }

}