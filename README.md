# PHP MedCoreSDK

PHP MedCore is a simple SDK implementation of MedCoreApi. It helps accessing the API in an object oriented way.

## Instalation
Install With Composer, add to composer file the next nodes or repositories

Repository seccion
```composer
"repositories": [
        {
            "url": "git@bitbucket.org:krapov-qwerty/medcore-conector.git",
            "type": "git"
        }
],
```

Require seccion 
```composer
"require": {
    +++ "medtrainer/medcore" : "dev-CRED-4328"
```

## Requeriments

MedCoreSDK uses curl extension for handling http calls.
So you need to have the curl extension installed and enabled with PHP.

##Usage
You can use MedCoreSDK in a pretty simple object oriented way.
Configure MedCore SDK

```php
$config = array(
    'url' => 'yourshop.medcore.com'
);
$medCoreSDK = new \MedCore\MedConnector($config);
```

### get the MedCoreSDK Object
```php
use MedCore\MedConnector;

$connector = new MedConnector($config);
```

- Get all Company list (GET request) Pagination

```php
$companies = $medCoreSDK->Company()->page(1);
```

- Get any specific Company with ID (GET request)

```php
$companyId = 2;
$company = $medCoreSDK->Company($companyId)->get();
```

- Update a Company (PUT Request)

```php
$updateInfo = array (
    "companyName" => "fulfilled",
);
$companyId = 2;
$medCoreSDK->Company($companyId)->put($updateInfo);
```