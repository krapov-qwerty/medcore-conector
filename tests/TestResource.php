<?php
declare(strict_types=1);
/**
 * MedTrainer
 * @copyright 2018
 * @author Ricardo Ruiz <rruiz@medtrainer.com>
 */
namespace MedCore;

use PHPUnit\Framework\TestCase;

/**
 * Class TestResource
 * @package MedCore
 */
class TestResource extends TestCase
{
    /**
     * @var MedConnector $medconnector;
     */
    public static $medconnector;

    /**
     * @inheritDoc
     */
    public static function setUpBeforeClass()
    {
        $config = array(
            'url' => 'http://core.local.com'
        );
        self::$medconnector = MedConnector::config($config);
        MedConnector::checkApiCallLimit();
    }
    public function  testCase1()
    {
        $this->assertTrue(true);
    }
    /**
     * @inheritDoc
     */
    public static function tearDownAfterClass()
    {
        self::$medconnector = null;
    }
}