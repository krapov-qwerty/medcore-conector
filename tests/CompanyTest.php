<?php
declare(strict_types=1);
/**
 * MedTrainer
 * @copyright 2018
 * @author Ricardo Ruiz <rruiz@medtrainer.com>
 */
namespace MedCore;

/**
 * Class ProductTest
 * @package MedCore
 */
class CompanyTest extends TestResource
{

    /**
     * TestSimpleResource constructor
     */
    public function __construct()
    {
        $this->resourceName = preg_replace('/.+\\\\(\w+)Test$/', '$1', get_called_class());
        parent::__construct();
    }
    /**
     * Test get resource
     * @group CompanyPagination
     */
    public function testGet()
    {
        $resource = static::$medconnector->{$this->resourceName};
        $result = $resource->get(array('partial'=> false));
        $this->assertTrue(is_array($result));
        if($resource->countEnabled) {
            //Count should match the result array count
            $count = static::$medconnector->{$this->resourceName}->count();
            $this->assertEquals($count, $result['hydra:totalItems']);
        }
    }
    /**
     * Test getting single resource by id
     * @group CompanyPagination
     */
    public function testGetSelf()
    {
        $id = 1;
        $company = static::$medconnector->{$this->resourceName}($id)->get();
        $this->assertTrue(is_array($company));
        $this->assertNotEmpty($company);
        $this->assertEquals($id, $company['id']);
    }
    /**
     * Test All the companies
     * @group CompanyPagination
     */
    public function testPaginator()
    {
        $items = 50;
        $resource = static::$medconnector->{$this->resourceName};
        $result = $resource->page(1, array('items'=>$items, "partial"=> false));
        $totalPages = ceil($result['hydra:totalItems'] / $items);
        for ($i = 2; $i <= $totalPages; $i++) {
            $result = $resource->page($i, array('items'=>$items));
            $this->assertTrue(is_array($result));
        }
    }
    /**
     * Test Get single Company
     * @group CompanyOperation
     */
    public function testGetCompany()
    {
        $resource = static::$medconnector->{$this->resourceName}(1);
        $company = $resource->get();
        $this->assertTrue(is_array($company));
    }
    /**
     * Test Get single Company
     * @group CompanyOperation
     */
    public function testPutCompany()
    {
        $resource = static::$medconnector->{$this->resourceName}(1);
        $company = $resource->get();
        $this->assertTrue(is_array($company));
        $valid  = $resource->put(array('companyName'=>'test'));
    }
    /**
     * Test Get single Company
     * @group CompanySettings
     */
    public function testGetCompanySettings()
    {
        $resource = static::$medconnector->{$this->resourceName}(197)->CompanySettings()->get();
        //$this->assertTrue(is_array($company));
    }
}